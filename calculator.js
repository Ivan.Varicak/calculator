const result = document.getElementById('result');
const operatorButtons = document.getElementsByClassName('operator');
const numberButtons = document.querySelectorAll('.keys button:not(.operator, .equal, .clear)');
const decimalButton = document.querySelector('.decimal');
const clearButton = document.querySelector('.clear');
const equalButton = document.querySelector('.equal');

let currentValue = 0;
let operator = '';
let shouldResetDisplay = false;

function resetCalculator() {
  currentValue = 0;
  operator = '';
  shouldResetDisplay = false;
  result.value = '0';
}

function handleNumberButtonClick(event) {
  const number = event.target.value;

  if (shouldResetDisplay) {
    result.value = number;
    shouldResetDisplay = false;
  } else {
    if (result.value === '0') {
      result.value = number;
    } else {
      result.value += number;
    }
  }
}

function handleOperatorButtonClick(event) {
  const nextOperator = event.target.value;

  if (operator && !shouldResetDisplay) {
    calculate();
  } else {
    currentValue = parseFloat(result.value);
  }

  operator = nextOperator;
  shouldResetDisplay = true;
}

function handleDecimalButtonClick() {
  if (shouldResetDisplay) {
    result.value = '0.';
    shouldResetDisplay = false;
  } else if (result.value.indexOf('.') === -1) {
    result.value += '.';
  }
}

function handleClearButtonClick() {
  resetCalculator();
}

function calculate() {
  const nextValue = parseFloat(result.value);

  if (operator === '+') {
    currentValue += nextValue;
  } else if (operator === '-') {
    currentValue -= nextValue;
  } else if (operator === '*') {
    currentValue *= nextValue;
  } else if (operator === '/') {
    currentValue /= nextValue;
  }

  result.value = currentValue.toString();
}

for (let i = 0; i < numberButtons.length; i++) {
  numberButtons[i].addEventListener('click', handleNumberButtonClick);
}

for (let i = 0; i < operatorButtons.length; i++) {
  operatorButtons[i].addEventListener('click', handleOperatorButtonClick);
}

decimalButton.addEventListener('click', handleDecimalButtonClick);

clearButton.addEventListener('click', handleClearButtonClick);

equalButton.addEventListener('click', calculate);
