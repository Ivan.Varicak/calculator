# Calculator



## Getting started

Hello everyone, here I have created a Calculator, so that you can use it, you must do the following:

1.Create a new folder on your machine and open a terminal 
If you use windows and don't know how to open a terminal, go to the website https://git-scm.com/download/win and download the version of git that corresponds to your machine
Installation is very easy, with a few clicks on next and accept you will be ready to use the terminal on your machine
Ps. some will have to restart the computer

2.In that folder, right-click on the white area and click on Git Bash Here
This will open a terminal and you will see the path to your folder

3.To download my project you have to go to my git repository https://gitlab.com/Ivan.Varicak
You need to find the CLONE button, which is usually located on the right side of the screen
When you click on CLONE, a window with more options will open, select the Clone with HTTPS option
It clones a link from my repository that will be copied to your terminal later

4.When we are back in the terminal, type the command GIT CLONE and copy "link from my repository"
In most cases, ctrl+c, ctrl+v in the terminal does not work, so you will have to right click and paste

5.Another folder called calcucaltor will be created in your folder
And to use my calculator, just open the calculator folder and double click on calculator.html



